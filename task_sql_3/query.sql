SELECT ClientID,
       SegmentID,
       STRFTIME('%Y-%m', MIN(date)) AS period_start,
       STRFTIME('%Y-%m', MAX(date)) AS period_end
FROM (SELECT *,
             month - ROW_NUMBER() OVER (ORDER BY ClientID, SegmentID, month) AS period
      FROM (SELECT ClientID,
                   SegmentID,
                   date,
                   CAST(STRFTIME('%Y', date) AS integer) * 12 +
                   CAST(STRFTIME('%m', date) AS integer) AS month
            FROM task3_segment))
GROUP BY ClientID, SegmentID, period;
