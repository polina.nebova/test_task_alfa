DROP TABLE task3_segment;
CREATE TABLE task3_segment
(
    [date]    date,
    ClientID  varchar(6),
    SegmentID int
);

INSERT INTO task3_segment
VALUES ('2018-01-31', 'A11111', 2),
       ('2018-02-28', 'A11111', 2),
       ('2018-03-31', 'A11111', 1),
       ('2018-04-30', 'A11111', 1),
       ('2017-11-30', 'B22222', 1),
       ('2017-10-31', 'B22222', 1),
       ('2017-09-30', 'B22222', 3),
       ('2017-09-30', 'C33333', 1),
       ('2017-10-31', 'C33333', 1);

