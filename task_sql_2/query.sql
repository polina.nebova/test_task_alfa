WITH with_month AS (
    SELECT *,
           STRFTIME('%m', date) AS month
    FROM task2_oper
)
SELECT b.date,
       SUM(a.cnt) AS total_cnt
FROM with_month AS a
         INNER JOIN with_month AS b
                    ON a.date <= b.date AND a.month = b.month
GROUP BY b.date;