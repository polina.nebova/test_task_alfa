SELECT client_id,
       SUM(Summa_USD) AS total_balance
FROM (SELECT client_id,
             MAX(Date),
             Summa_USD
      FROM task1_score AS score
               INNER JOIN
           (SELECT client_id,
                   account_num
            FROM task1_clients
            WHERE Region = 'Москва') AS clients_msk
           ON score.Account_num = clients_msk.account_num
      GROUP BY client_id,
               score.Account_num)
GROUP BY client_id
HAVING total_balance >= 20000;