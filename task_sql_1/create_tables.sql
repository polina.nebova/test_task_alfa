CREATE TABLE task1_clients
(
    client_id   varchar(1),
    FIO         varchar(255),
    Region      varchar(50),
    account_num int
);
INSERT INTO task1_clients
VALUES ('A', 'Иванов', 'Москва', 111),
       ('A', 'Иванов', 'Москва', 222),
       ('B', 'Петров', 'Иваново', 333),
       ('C', 'Сидоров', 'Москва', 444);

DROP TABLE task1_score;
CREATE TABLE task1_score
(
    [Date]      date,
    Summa_USD   money,
    Account_num int
);
INSERT INTO task1_score
VALUES ('2012-01-01', 15000, 111),
       ('2012-02-01', 10000, 111),
       ('2012-02-01', 5000, 222),
       ('2012-03-01', 30000, 333),
       ('2012-04-01', 20000, 444);
